from keras.datasets import mnist
from keras.utils import to_categorical
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD, RMSprop
from keras.regularizers import l2
from keras.initializers import lecun_normal
import datetime

import os

#model.fit(x_train, y_train, validation_data=(x_val,y_val), epochs=3, batch_size=32)

'''
    fisier de test
'''


import time
import copy
import random
from pprint import pprint
import pygame
import pygame.event
from pygame.rect import Rect
from pygame.locals import (

    K_UP,

    K_DOWN,

    K_LEFT,

    K_RIGHT,

    K_ESCAPE,

    K_SPACE,

    KEYDOWN,

    QUIT,

    KEYUP,



)

pygame.init()

screen = pygame.display.set_mode((800, 600))
menu_screen = pygame.display.set_mode((800, 600))
caption = pygame.display.set_caption("iuli")

circle = dict()

circle['color'] = (100, 50, 150)
circle['center'] = (400, 300)
circle['radius'] = 25

movement = dict()

movement[K_UP] = (0, -1)
movement[K_DOWN] = (0, 1)
movement[K_LEFT] = (-1, 0)
movement[K_RIGHT] = (1, 0)

pieces = dict()

# pieces[0] = np.array([[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]])
# pieces[1] = np.array([[0, 0, 0, 0], [0, 0, 1, 0], [0, 1, 1, 1], [0, 0, 0, 0]])
# pieces[2] = np.array([[0, 0, 0, 0], [0, 0, 1, 1], [0, 1, 1, 0], [0, 0, 0, 0]])
# pieces[3] = np.array([[0, 0, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1], [0, 0, 0, 0]])
# pieces[4] = np.array([[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0]])
# pieces[5] = np.array([[0, 0, 1, 0], [0, 0, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]])

pieces[0] = [[0, 5], [1, 5], [2, 5], [3, 5]]
pieces[1] = [[0, 4], [0, 5], [1, 5], [1, 6]]
pieces[2] = [[0, 5], [0, 6], [1, 4], [1, 5]]
pieces[3] = [[0, 5], [1, 4], [1, 5], [1, 6]]
pieces[4] = [[0, 4], [1, 4], [1, 5], [1, 6]]
pieces[5] = [[0, 6], [1, 4], [1, 5], [1, 6]]
pieces[6] = [[0, 4], [0, 5], [1, 4], [1, 5]]

trans = dict()

trans[0] = [[[2, 1], [1, 0], [0, -1], [-1, -2]], [[1, -2], [0, -1], [-1, 0], [-2, 1]], [[-2, -1], [-1, 0], [0, 1], [1, 2]], [[-1, 2], [0, 1], [1, 0], [2, -1]]]
trans[1] = [[[0, 2], [1, 1], [0, 0], [1, -1]], [[2, 0], [1, -1], [0, 0], [-1, -1]], [[0, -2], [-1, -1], [0, 0], [-1, 1]], [[-2, 0], [-1, 1], [0, 0], [1, 1]]]
trans[2] = [[[1, 1], [2, 0], [-1, 1], [0, 0]], [[1, -1], [0, -2], [1, 1], [0, 0]], [[-1, -1], [-2, 0], [1, -1], [0, 0]], [[-1, 1], [0, 2], [-1, -1], [0, 0]]]
trans[3] = [[[1, 1], [-1, 1], [0, 0], [1, -1]], [[1, -1], [1, 1], [0, 0], [-1, -1]], [[-1, -1], [1, -1], [0, 0], [-1, 1]], [[-1, 1], [-1, -1], [0, 0], [1, 1]]]
trans[4] = [[[0, 2], [-1, 1], [0, 0], [1, -1]], [[2, 0], [1, 1], [0, 0], [-1, -1]], [[0, -2], [1, -1], [0, 0], [-1, 1]], [[-2, 0], [-1, -1], [0, 0], [1, 1]]]
trans[5] = [[[2, 0], [-1, 1], [0, 0], [1, -1]], [[0, -2], [1, 1], [0, 0], [-1, -1]], [[-2, 0], [1, -1], [0, 0], [-1, 1]], [[0, 2], [-1, -1], [0, 0], [1, 1]]]

colors = dict()

colors[0] = pygame.Color('red')
colors[1] = pygame.Color('blue')
colors[2] = pygame.Color('yellow')
colors[3] = pygame.Color('green')
colors[4] = pygame.Color('cyan')
colors[5] = pygame.Color('black')
colors[6] = pygame.Color('orange')

font = pygame.font.SysFont('Arial', 25)

score = 0
line_score = 100
line_reward = 20

game_speed = 0.03
table = np.zeros((23, 10))


def move_circle(circ, poz):
    circ['center'] = (circ['center'][0] + movement[poz][0], circ['center'][1] + movement[poz][1])

def increase_circle_size(circ):
    circ['radius'] += 1

def argmax(keyz):
    ret_list = []
    for i in range(273, 277):
        if keyz[i] == 1:
            ret_list.append(i)
    return ret_list



def is_inside_rect(pos, rect):
    if pos[0] >= rect[0] + rect[2]:
        return False
    if pos[1] >= rect[1] + rect[3]:
        return False
    if pos[0] < rect[0] or pos[1] < rect[1]:
        return False
    return True


def move_down(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][0] += 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def move_left(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][1] -= 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def move_right(piece, tbl, piece_nr):

    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][1] += 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def can_go_down(piece, tbl):
    for pos in piece:
        if pos[0] == 22:
            return False
        if tbl[pos[0] + 1][pos[1]] > 0:
            return False
    return True

def can_go_left(piece, tbl):
    for pos in piece:
        if pos[1] == 0:
            return False
        if tbl[pos[0]][pos[1] - 1] > 0:
            return False
    return True

def can_go_right(piece, tbl):
    for pos in piece:
        if pos[1] == 9:
            return False
        if tbl[pos[0]][pos[1] + 1] > 0:
            return False
    return True

def verify_if_about_to_exit_right(piece):
    for pos in piece:
        if pos[1] == 9:
            return False
    return True

def verify_if_about_to_exit_left_by_one(piece):
    for pos in piece:
        if pos[1] == 0:
            return False

    return True

def verify_if_about_to_exit_left_by_two(piece):
    for pos in piece:
        if pos[1] < 2:
            return False
    return True

def rotate(piece, state, piece_nr, tbl):
    cpy_piece = copy.deepcopy(piece)
    for i, pos in enumerate(piece):
        piece[i][0] += trans[piece_nr][state % 4][i][0]
        piece[i][1] += trans[piece_nr][state % 4][i][1]

    for i, pos in enumerate(piece):
        if piece[i][0] > 22:
            for j in range(len(piece)):
                piece[j][0] -= 1
        if piece[i][0] < 0:
            for j in range(len(piece)):
                piece[j][0] += 1

    for i, pos in enumerate(piece):
        if piece[i][1] < 0:
            for j in range(len(piece)):
                piece[j][1] += 1

        if piece[i][1] > 9:
            for j in range(len(piece)):
                piece[j][1] -= 1

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 and verify_if_about_to_exit_right(piece):
            #print('mergem la dreapta')
            for j in range(len(piece)):
                piece[j][1] += 1
            for j, poz in enumerate(piece):
                if poz[1] > 9:
                    return cpy_piece, state
                if tbl[poz[0]][poz[1]] > 0 and verify_if_about_to_exit_right(piece):
                    #print('mai mergem o data')
                    for k in range(len(piece)):
                        piece[k][1] += 1
                    break
            break

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 and verify_if_about_to_exit_left_by_two(piece):
            #print('mergem la stanga')
            for j in range(len(piece)):
                piece[j][1] -= 2
            for j, poz in enumerate(piece):
                if tbl[poz[0]][poz[1]] > 0 and verify_if_about_to_exit_left_by_one(piece):
                    #print('mai mergem o data')
                    for k in range(len(piece)):
                        piece[k][1] -= 1
                    break
            break

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 or piece[i][1] > 9 or piece[i][1] < 0:
            return cpy_piece, state

    state += 1 % 4
    return piece, state

def put_piece_on_table(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for pos in piece:
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl




def generate_new_piece():
    piece_nr = random.randint(0, 6)
    return piece_nr, copy.deepcopy(pieces[piece_nr])


def apply_combo(nr):
    if nr == 0:
        return 0
    else:
        p = 1
        for i in range(1, nr+1):
            p *= i

        return p

def verify_full_line(tbl):
    tmp_tbl = tbl
    combo = 0
    global score
    global line_score
    global game_speed
    global line_reward

    for i, line in enumerate(tmp_tbl):
        bool_var = False
        for j in line:
            if j == 0:
                bool_var = True
        if not bool_var:
            combo += 1
            tmp_tbl = np.delete(tmp_tbl, i, axis=0)
            tmp_tbl = np.insert(tmp_tbl, 0, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], axis=0)

    combo = apply_combo(combo)
    """ if combo > 0 and (score + line_score*combo) // 500 != score // 500:
        if game_speed > 0.5:
            game_speed -= 0.1
        elif game_speed > 0.01:
            game_speed -= 0.01 """
     
    score += line_score*combo

    return tmp_tbl, line_reward*combo

def go_full_down(piece, tbl, temp_tbl, piece_nr):
    while can_go_down(piece, tbl):
        temp_tbl, piece = move_down(piece, tbl, piece_nr)
    return temp_tbl, piece

def get_state(table, piece, piece_number):
    # return np.concatenate((((table+6)//7).reshape(230,1), np.array(piece).reshape(8,1))).reshape(1,238) fara numarul piesei viitoare
    return np.concatenate((((table+6)//7).reshape(230,1), np.array(piece).reshape(8,1), np.array([piece_number/5]).reshape(1,1))).reshape(1,239)

def get_neighbours(table, piece):
    neigh = set()
    
    for x,y in piece:
        if (x-1 >= 0 and table[x-1][y] == 1 and [x-1,y] not in piece): # or x-1 < 0:
            neigh.add((x-1,y))
        if (x+1 < table.shape[0] and table[x+1][y] == 1 and [x+1,y] not in piece): # or x+1 > 22:
            neigh.add((x+1,y))
        if (y-1 >= 0 and table[x-1][y] == 1 and [x,y-1] not in piece): # or y-1 < 0:
            neigh.add((x,y-1))
        if (y+1 < table.shape[1] and table[x][y+1] == 1 and [x,y+1] not in piece): # or y+1 > 9:
            neigh.add((x, y+1))

    return len(neigh)
            
def get_empty_places(table, piece):
    neigh = set()
    
    for x,y in piece:
        if (x-1 >= 0 and table[x-1][y] == 0 and [x-1,y] not in piece): # or x-1 < 0:
            neigh.add((x-1,y))
        if (x+1 < table.shape[0] and table[x+1][y] == 0 and [x+1,y] not in piece): # or x+1 > 22:
            neigh.add((x+1,y))
        if (y-1 >= 0 and table[x-1][y] == 0 and [x,y-1] not in piece): # or y-1 < 0:
            neigh.add((x,y-1))
        if (y+1 < table.shape[1] and table[x][y+1] == 0 and [x,y+1] not in piece): # or y+1 > 9:
            neigh.add((x, y+1))

    return neigh

            
def isSafe(i, j, visited, t): 
    return (i >= 0 and i < t.shape[0] and 
            j >= 0 and j < t.shape[1] and 
            not visited[i][j] and t[i][j] == 0) 
              
def DFS(i, j, visited, t, count): 
    rowNbr = [-1,  0, 0, 1]; 
    colNbr = [ 0, -1, 1, 0]; 
        
    visited[i][j] = count+1

    for k in range(4): 
        if isSafe(i + rowNbr[k], j + colNbr[k], visited, t): 
            DFS(i + rowNbr[k], j + colNbr[k], visited, t, count) 

def no_holes(table, piece): 
    visited = [[0 for j in range(table.shape[1])]for i in range(table.shape[0])] 

    count = 0
    
    for x,y in get_empty_places(table, piece):
        if visited[x][y] == 0 and table[x][y] == 0:
            DFS(x, y, visited, table, count)
            count += 1

        #pprint(visited)

    excluded = {0}
    height = table.shape[1]

    for y in range(table.shape[1]):
        excluded.add(visited[0][y])

    return sum([sum([0 if visited[i][j] in excluded else 1-j/height for j in range(table.shape[1])]) for i in range(table.shape[0])])

def penalty(table, piece):
    #print(table)
    max_height = 0
    for row in table:
        for el in row:
            if el != 0:
                break
        max_height += 1

    a = no_holes(table,piece)
    #print("Ii scad", a)
    return -sum([24-x[0] for x in piece])/4 + get_neighbours(table, piece) - a
"""
input - state:
    23x10 pozitii ale tablei = 230
    8 pozitii ale blocului in miscare

output:
    valoarea Q(s, a) pentru fiecare actiune

actiuni:
    stanga
    dreapta
    sus (rotire)
    jos 
    space - optionale
    do_nothing ? (in locul celor 2)
"""

import pickle

model = Sequential()
# model.add(Dropout(0.2))
model.add(Dense(200, activation='relu', kernel_regularizer=l2(1e-4), kernel_initializer=lecun_normal()))
model.add(Dense(4, activation='linear', kernel_regularizer=l2(1e-4), kernel_initializer=lecun_normal()))

rms = RMSprop(lr=0.01)

sgd = SGD(lr=0.1, momentum=0.9, nesterov=True)# , loss='categorical_crossentropy')
#model.compile(optimizer=sgd, loss='categorical_crossentropy')
model.compile(optimizer=rms, loss='mse')# SGD(lr=0.1, momentum=0.9, nesterov=True), loss='categorical_crossentropy', metrics=['acc'])
#

epochs = 5000
gamma = 0.9
epsilon = 0.95
replay = []
replay_size = 50000
batchSize = 40
h = 0
standard_reward = -1.0
action_map = [pygame.event.Event(pygame.KEYDOWN, key=K_LEFT),
            pygame.event.Event(pygame.KEYDOWN, key=K_UP),
            pygame.event.Event(pygame.KEYDOWN, key=K_RIGHT),
            pygame.event.Event(pygame.KEYDOWN, key=K_DOWN)]

start_time = time.time()

data_start = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

os.mkdir(data_start)

for ep in range(epochs):
    table = np.zeros((23, 10))
    rectangle = Rect((300, 200), (200, 150))

    # nu vom folosi game_state 1 pentru ai
    game_state = 2

    pis_number = random.randint(0, 5)
    pis = copy.deepcopy(pieces[pis_number])
    pis_state = 0

    next_pis_number = random.randint(0,5)
    next_pis = copy.deepcopy(pieces[next_pis_number])
    next_pis_state = 0


    ref_time = time.time()
    temp_table = copy.deepcopy(table)

    # next_pis -> incercam sa antrenam stiind piesa pe care o va lua data urmatoare

    state = get_state(table, pis, next_pis_number)
    print(epsilon)

    while game_state > 0:
        qvals = model.predict(state, batch_size=1)

        if random.random() < epsilon:
            action = np.random.randint(0,4)
        else:
            action = np.argmax(qvals)

        #print(action)
        pygame.event.post(action_map[action])
        
        if game_state == 1:
            menu_screen.fill((10, 99, 200))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_state = 0
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if is_inside_rect(event.pos, rectangle):
                        ref_time = time.time()
                        game_state = 2
                if event.type == pygame.KEYDOWN:
                    if event.key == K_ESCAPE:
                        game_state = 0

            pygame.draw.rect(menu_screen, (255, 255, 255), rectangle)

        if game_state == 2:
            screen.fill((120, 150, 255))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_state = 0
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        game_state = 1
                    if event.key == K_LEFT:
                        if can_go_left(pis, table):
                            temp_table, pis = move_left(pis, table, pis_number)
                            reward = standard_reward
                        else:
                            reward = 2*standard_reward
                    if event.key == K_RIGHT:
                        if can_go_right(pis, table):
                            temp_table, pis = move_right(pis, table, pis_number)
                            reward = standard_reward
                        else:
                            reward = standard_reward
                    if event.key == K_UP:
                        if pis_number < 6:
                            pis, pis_state = rotate(pis, pis_state, pis_number, table)
                            temp_table = put_piece_on_table(pis, table, pis_number)
                            reward = standard_reward
                    if event.key == K_DOWN:
                        # incurajam sa se grabeasca
                        if can_go_down(pis, table):
                            temp_table, pis = move_down(pis, table, pis_number)
                            reward = standard_reward
                        else:
                            temp_table, reward = verify_full_line(temp_table)
                            reward += standard_reward + penalty(temp_table,pis)
                            pis_number, pis = next_pis_number, next_pis
                            next_pis_number, next_pis = generate_new_piece()
                            pis_state = 0
                            table = copy.deepcopy(temp_table)
                        #reward += 0.5                   

                    if event.key == K_SPACE:
                        temp_table, pis = go_full_down(pis, table, temp_table, pis_number)
                        temp_table, reward = verify_full_line(temp_table)
                        pis_number, pis = next_pis_number, next_pis
                        next_pis_number, next_pis = generate_new_piece()
                        pis_state = 0
                        table = copy.deepcopy(temp_table)


            for i, line in enumerate(temp_table):
                for j, square in enumerate(line):
                    if square:
                        pygame.draw.rect(screen, colors[temp_table[i][j] - 1], Rect((25 * (j + 10), 25 * (i + 1)), (25, 25)))
            pygame.draw.rect(screen,(255,255,255), Rect((0,0), (250,600)))
            pygame.draw.rect(screen,(255,255,255), Rect((500,0), (300,600)))
            screen.blit(font.render("Score", True, (0,0,0)), (600, 0))
            screen.blit(font.render(str(score), True, (255,0,0)), (625, 25))
            screen.blit(font.render("Epoca", True, (0,0,0)), (600, 50))
            screen.blit(font.render(str(ep), True, (255,0,0)), (625, 75))
            screen.blit(font.render("Next piece", True, (0,0,0)), (600, 100))
            for i,j in next_pis:
                pygame.draw.rect(screen, colors[next_pis_number], Rect((550+25*j, 250+25*i), (25, 25)))            

            

            curr_time = time.time()
            if curr_time - ref_time > game_speed:
                ref_time = curr_time
                if can_go_down(pis, table):
                    temp_table, pis = move_down(pis, table, pis_number)
                else:
                    temp_table, r = verify_full_line(temp_table)
                    table = copy.deepcopy(temp_table)
                    reward += penalty(table,pis)+r
                    pis_number, pis = next_pis_number, next_pis
                    next_pis_number, next_pis = generate_new_piece()
                    pis_state = 0
                    

            # print(reward)

            extra_elapsed = time.time()
            new_state = get_state(table, pis, next_pis_number)

            if sum(table[0]) + sum(table[1]) + sum(table[2]) > 0:
                game_state = 0
                print('game over')
                reward -= 500

            if (len(replay) < replay_size): #if buffer not filled, add to it
                replay.append((state, action, reward, new_state))
            else: #if buffer full, overwrite old values
                a = time.time()

                if (h < (replay_size-1)):
                    h += 1
                else:
                    h = 0
                replay[h] = (state, action, reward, new_state)
                #randomly sample our experience replay memory
                # print(replay)
                
                minibatch = random.sample(replay, batchSize)
                X_train = []
                y_train = []
                
                for memory in minibatch:
                    #Get max_Q(S',a)
                    old_state, action, reward, new_state = memory
                    old_qval = model.predict(old_state, batch_size=1)
                    # print(old_qval.shape)
                    newQ = model.predict(new_state, batch_size=1)
                    maxQ = np.max(newQ)
                    y = np.zeros((1,4))
                    y[:] = old_qval[:]
                    if game_state != 0: #non-terminal state
                        update = (reward + (gamma * maxQ))
                    else: #terminal state
                        update = reward
                    y[0][action] = update
                    X_train.append(old_state.reshape(239,)) # 238 in cazul fara piesa viitoare
                    y_train.append(y.reshape(4,))
                    
                
                X_train = np.array(X_train)
                y_train = np.array(y_train)
                
                model.fit(X_train, y_train, batch_size=batchSize, epochs=1, verbose=0)
                state = new_state

                # print("Elapsed: {}".format(time.time()-a))

            ref_time += time.time()-extra_elapsed
                
               
        pygame.display.flip()
        """ 
        if time.time() - start_time >= 60*10:
            start_time = time.time()
            with open(datetime.datetime.now().strftime("%Y%m%d_%H%M")+"-replay.txt", "wb") as f:
                pickle.dump(replay, f)
            
            model.save_weights(datetime.datetime.now().strftime("%Y%m%d_%H%M")) 
        """


    print("Game #: {} {}".format(ep,score))

    if ep % 50 == 0:
        file_name = os.path.join(data_start, "epoch{}".format(ep))
        with open(file_name + "-replay.txt", "wb") as f:
            pickle.dump(replay, f)
            
        model.save_weights(file_name)

    if epsilon > 0.1: #decrement epsilon over time
        epsilon -= (1/(epochs))




""" 
# salvam weighturile
model.save_weights(datetime.datetime.now().strftime("%Y%m%d_%H%M"))
pygame.quit()

# salvam replay-urile
with open(datetime.datetime.now().strftime("%Y%m%d_%H%M")+".txt", "wb") as f:
    pickle.dump(replay, f) """