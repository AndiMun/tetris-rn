'''
    fisier de test
'''


import time
import copy
import random

import pygame
from pygame.rect import Rect
import numpy as np
from pygame.locals import (

    K_UP,

    K_DOWN,

    K_LEFT,

    K_RIGHT,

    K_ESCAPE,

    K_SPACE,

    KEYDOWN,

    QUIT,

    KEYUP,



)

pygame.init()

screen = pygame.display.set_mode((800, 600))
menu_screen = pygame.display.set_mode((800, 600))
caption = pygame.display.set_caption("iuli")

circle = dict()

circle['color'] = (100, 50, 150)
circle['center'] = (400, 300)
circle['radius'] = 25

movement = dict()

movement[K_UP] = (0, -1)
movement[K_DOWN] = (0, 1)
movement[K_LEFT] = (-1, 0)
movement[K_RIGHT] = (1, 0)

pieces = dict()

# pieces[0] = np.array([[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]])
# pieces[1] = np.array([[0, 0, 0, 0], [0, 0, 1, 0], [0, 1, 1, 1], [0, 0, 0, 0]])
# pieces[2] = np.array([[0, 0, 0, 0], [0, 0, 1, 1], [0, 1, 1, 0], [0, 0, 0, 0]])
# pieces[3] = np.array([[0, 0, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1], [0, 0, 0, 0]])
# pieces[4] = np.array([[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0]])
# pieces[5] = np.array([[0, 0, 1, 0], [0, 0, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]])

pieces[0] = [[0, 5], [1, 5], [2, 5], [3, 5]]
pieces[1] = [[0, 4], [0, 5], [1, 5], [1, 6]]
pieces[2] = [[0, 5], [0, 6], [1, 4], [1, 5]]
pieces[3] = [[0, 5], [1, 4], [1, 5], [1, 6]]
pieces[4] = [[0, 4], [1, 4], [1, 5], [1, 6]]
pieces[5] = [[0, 6], [1, 4], [1, 5], [1, 6]]
pieces[6] = [[0, 4], [0, 5], [1, 4], [1, 5]]

trans = dict()

trans[0] = [[[2, 1], [1, 0], [0, -1], [-1, -2]], [[1, -2], [0, -1], [-1, 0], [-2, 1]], [[-2, -1], [-1, 0], [0, 1], [1, 2]], [[-1, 2], [0, 1], [1, 0], [2, -1]]]
trans[1] = [[[0, 2], [1, 1], [0, 0], [1, -1]], [[2, 0], [1, -1], [0, 0], [-1, -1]], [[0, -2], [-1, -1], [0, 0], [-1, 1]], [[-2, 0], [-1, 1], [0, 0], [1, 1]]]
trans[2] = [[[1, 1], [2, 0], [-1, 1], [0, 0]], [[1, -1], [0, -2], [1, 1], [0, 0]], [[-1, -1], [-2, 0], [1, -1], [0, 0]], [[-1, 1], [0, 2], [-1, -1], [0, 0]]]
trans[3] = [[[1, 1], [-1, 1], [0, 0], [1, -1]], [[1, -1], [1, 1], [0, 0], [-1, -1]], [[-1, -1], [1, -1], [0, 0], [-1, 1]], [[-1, 1], [-1, -1], [0, 0], [1, 1]]]
trans[4] = [[[0, 2], [-1, 1], [0, 0], [1, -1]], [[2, 0], [1, 1], [0, 0], [-1, -1]], [[0, -2], [1, -1], [0, 0], [-1, 1]], [[-2, 0], [-1, -1], [0, 0], [1, 1]]]
trans[5] = [[[2, 0], [-1, 1], [0, 0], [1, -1]], [[0, -2], [1, 1], [0, 0], [-1, -1]], [[-2, 0], [1, -1], [0, 0], [-1, 1]], [[0, 2], [-1, -1], [0, 0], [1, 1]]]

colors = dict()

colors[0] = pygame.Color('red')
colors[1] = pygame.Color('blue')
colors[2] = pygame.Color('yellow')
colors[3] = pygame.Color('green')
colors[4] = pygame.Color('white')
colors[5] = pygame.Color('black')
colors[6] = pygame.Color('orange')

font = pygame.font.SysFont('Arial', 25)

score = 0
line_score = 100

game_speed = 1.5
table = np.zeros((23, 10))


def move_circle(circ, poz):
    circ['center'] = (circ['center'][0] + movement[poz][0], circ['center'][1] + movement[poz][1])

def increase_circle_size(circ):
    circ['radius'] += 1

def argmax(keyz):
    ret_list = []
    for i in range(273, 277):
        if keyz[i] == 1:
            ret_list.append(i)
    return ret_list



def is_inside_rect(pos, rect):
    if pos[0] >= rect[0] + rect[2]:
        return False
    if pos[1] >= rect[1] + rect[3]:
        return False
    if pos[0] < rect[0] or pos[1] < rect[1]:
        return False
    return True


def move_down(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][0] += 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def move_left(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][1] -= 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def move_right(piece, tbl, piece_nr):

    temp_tbl = copy.deepcopy(tbl)
    for i, pos in enumerate(piece):
        piece[i][1] += 1
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl, piece

def can_go_down(piece, tbl):
    for pos in piece:
        if pos[0] == 22:
            return False
        if tbl[pos[0] + 1][pos[1]] > 0:
            return False
    return True

def can_go_left(piece, tbl):
    for pos in piece:
        if pos[1] == 0:
            return False
        if tbl[pos[0]][pos[1] - 1] > 0:
            return False
    return True

def can_go_right(piece, tbl):
    for pos in piece:
        if pos[1] == 9:
            return False
        if tbl[pos[0]][pos[1] + 1] > 0:
            return False
    return True

def verify_if_about_to_exit_right(piece):
    for pos in piece:
        if pos[1] == 9:
            return False
    return True

def verify_if_about_to_exit_left_by_one(piece):
    for pos in piece:
        if pos[1] == 0:
            return False

    return True

def verify_if_about_to_exit_left_by_two(piece):
    for pos in piece:
        if pos[1] < 2:
            return False
    return True

def rotate(piece, state, piece_nr, tbl):
    cpy_piece = copy.deepcopy(piece)
    for i, pos in enumerate(piece):
        piece[i][0] += trans[piece_nr][state % 4][i][0]
        piece[i][1] += trans[piece_nr][state % 4][i][1]

    for i, pos in enumerate(piece):
        if piece[i][0] > 22:
            for j in range(len(piece)):
                piece[j][0] -= 1
        if piece[i][0] < 0:
            for j in range(len(piece)):
                piece[j][0] += 1

    for i, pos in enumerate(piece):
        if piece[i][1] < 0:
            for j in range(len(piece)):
                piece[j][1] += 1

        if piece[i][1] > 9:
            for j in range(len(piece)):
                piece[j][1] -= 1

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 and verify_if_about_to_exit_right(piece):
            print('mergem la dreapta')
            for j in range(len(piece)):
                piece[j][1] += 1
            for j, poz in enumerate(piece):
                if poz[1] > 9:
                    return cpy_piece, state
                if tbl[poz[0]][poz[1]] > 0 and verify_if_about_to_exit_right(piece):
                    print('mai mergem o data')
                    for k in range(len(piece)):
                        piece[k][1] += 1
                    break
            break

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 and verify_if_about_to_exit_left_by_two(piece):
            print('mergem la stanga')
            for j in range(len(piece)):
                piece[j][1] -= 2
            for j, poz in enumerate(piece):
                if tbl[poz[0]][poz[1]] > 0 and verify_if_about_to_exit_left_by_one(piece):
                    print('mai mergem o data')
                    for k in range(len(piece)):
                        piece[k][1] -= 1
                    break
            break

    for i, pos in enumerate(piece):
        if tbl[pos[0]][pos[1]] > 0 or piece[i][1] > 9 or piece[i][1] < 0:
            return cpy_piece, state

    state += 1 % 4
    return piece, state

def put_piece_on_table(piece, tbl, piece_nr):
    temp_tbl = copy.deepcopy(tbl)
    for pos in piece:
        temp_tbl[pos[0]][pos[1]] = piece_nr + 1
    return temp_tbl




def generate_new_piece():
    piece_nr = random.randint(0, 6)
    return piece_nr, copy.deepcopy(pieces[piece_nr])


def apply_combo(nr):
    if nr == 0:
        return 0
    else:
        p = 1
        for i in range(1, nr+1):
            p *= i

        return p

def verify_full_line(tbl):
    tmp_tbl = tbl
    combo = 0
    global score
    global line_score
    global game_speed

    for i, line in enumerate(tmp_tbl):
        bool_var = False
        for j in line:
            if j == 0:
                bool_var = True
        if not bool_var:
            combo += 1
            tmp_tbl = np.delete(tmp_tbl, i, axis=0)
            tmp_tbl = np.insert(tmp_tbl, 0, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], axis=0)

    combo = apply_combo(combo)
    if combo > 0 and (score + line_score*combo) // 500 != score // 500:
        if game_speed > 0.5:
            game_speed -= 0.1
        elif game_speed > 0.01:
            game_speed -= 0.01
     
    score += line_score*combo

    return tmp_tbl

def go_full_down(piece, tbl, temp_tbl, piece_nr):
    while can_go_down(piece, tbl):
        temp_tbl, piece = move_down(piece, tbl, piece_nr)
    return temp_tbl, piece




rectangle = Rect((300, 200), (200, 150))

game_state = 1

pis = copy.deepcopy(pieces[0])
pis_state = 0
pis_number = 0

ref_time = None
temp_table = copy.deepcopy(table)

while game_state > 0:
    if game_state == 1:
        menu_screen.fill((10, 99, 200))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_state = 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                if is_inside_rect(event.pos, rectangle):
                    ref_time = time.time()
                    game_state = 2
            if event.type == pygame.KEYDOWN:
                if event.key == K_ESCAPE:
                    game_state = 0

        pygame.draw.rect(menu_screen, (255, 255, 255), rectangle)

    if game_state == 2:
        screen.fill((120, 150, 255))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_state = 0
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    game_state = 1
                if event.key == K_LEFT:
                    if can_go_left(pis, table):
                        temp_table, pis = move_left(pis, table, pis_number)
                if event.key == K_RIGHT:
                    if can_go_right(pis, table):
                        temp_table, pis = move_right(pis, table, pis_number)
                if event.key == K_UP:
                    if pis_number < 6:
                        pis, pis_state = rotate(pis, pis_state, pis_number, table)
                        temp_table = put_piece_on_table(pis, table, pis_number)
                if event.key == K_DOWN:
                    if can_go_down(pis, table):
                        temp_table, pis = move_down(pis, table, pis_number)
                    else:
                        temp_table = verify_full_line(temp_table)
                        pis_number, pis = generate_new_piece()
                        pis_state = 0
                        table = copy.deepcopy(temp_table)

                if event.key == K_SPACE:
                    temp_table, pis = go_full_down(pis, table, temp_table, pis_number)
                    temp_table = verify_full_line(temp_table)
                    pis_number, pis = generate_new_piece()
                    pis_state = 0
                    table = copy.deepcopy(temp_table)



        for i, line in enumerate(temp_table):
            for j, square in enumerate(line):
                if square:
                    pygame.draw.rect(screen, colors[temp_table[i][j] - 1], Rect((25 * (j + 10), 25 * (i + 1)), (25, 25)))
        pygame.draw.rect(screen,(255,255,255), Rect((0,0), (250,600)))
        pygame.draw.rect(screen,(255,255,255), Rect((500,0), (300,600)))
        screen.blit(font.render("Score", True, (0,0,0)), (600, 0))
        screen.blit(font.render(str(score), True, (255,0,0)), (625, 25))


        curr_time = time.time()
        if curr_time - ref_time > game_speed:
            ref_time = curr_time
            if can_go_down(pis, table):
                temp_table, pis = move_down(pis, table, pis_number)
            else:
                temp_table = verify_full_line(temp_table)
                table = copy.deepcopy(temp_table)
                pis_number, pis = generate_new_piece()
                pis_state = 0
    if sum(table[0]) + sum(table[1]) + sum(table[2]) > 0:
        game_state = 0
        print('game over')
    pygame.display.flip()


pygame.quit()
